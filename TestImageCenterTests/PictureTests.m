//
//  PictureTests.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "Picture+Model.h"



@interface PictureTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    Picture *sut;
}

@end


@implementation PictureTests

#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    [self createCoreDataStack];
    [self createFixture];
    [self createSut];
}


- (void) createCoreDataStack {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
}


- (void) createFixture {
    // Test data
}


- (void) createSut {
    NSDictionary *dict = @{@"url":@"01.jpg",@"name":@"Sea",@"size":@123};
    sut = [Picture insertPictureInMOC:context withDict:dict];
}


- (void) tearDown {
    [self releaseSut];
    [self releaseFixture];
    [self releaseCoreDataStack];

    [super tearDown];
}


- (void) releaseSut {
    sut = nil;
}


- (void) releaseFixture {

}


- (void) releaseCoreDataStack {
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    // Prepare

    // Operate

    // Check
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}

- (void)testFetchAllReturnsOnePicture {
    NSArray *results = [Picture fetchAllPicturesInMOC:context];
    XCTAssertTrue([results count] == 1, @"The array must content 1 picture");
}

@end
