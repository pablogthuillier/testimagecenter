//
//  PGTPictureViewModel.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Picture+Model.h"

@interface PGTPictureViewModel : NSObject

@property (nonatomic,strong) Picture *picture;

@property (nonatomic,copy) NSString *pictureName;
@property (nonatomic,strong) UIImage *pictureImage;
@property (nonatomic) NSUInteger pictureSize;
@property (nonatomic,copy) NSString *pictureDate;

@end
