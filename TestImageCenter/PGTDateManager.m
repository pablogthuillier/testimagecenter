//
//  PGTDateManager.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDateManager.h"

@implementation PGTDateManager

+ (NSString *)dateWithDayMonthAndYear:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YY"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)localStringFromDate:(NSDate *)date {
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
}

@end
