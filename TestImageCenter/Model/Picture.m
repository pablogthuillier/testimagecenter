//
//  Picture.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Picture.h"


@implementation Picture

@dynamic name;
@dynamic date;
@dynamic url;
@dynamic size;

@end
