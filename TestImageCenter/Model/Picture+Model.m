//
//  Picture+Model.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Picture+Model.h"

NSString *const pictureEntityName = @"Picture";
NSString *const nameProperty = @"name";
NSString *const sizeProperty = @"size";
NSString *const dateProperty = @"date";
NSString *const urlProperty = @"url";

@implementation Picture (Model)

+ (instancetype)insertPictureInMOC:(NSManagedObjectContext *)context withDict:(NSDictionary *)dict {
    Picture *pict = [NSEntityDescription insertNewObjectForEntityForName:pictureEntityName inManagedObjectContext:context];
    pict.name = dict[nameProperty];
    pict.size = dict[sizeProperty];
    pict.date = [NSDate date];
    pict.url = dict[urlProperty];
    
    return pict;
    
}

+ (NSArray *)fetchAllPicturesInMOC:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:pictureEntityName];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:dateProperty ascending:NO]];
    NSError *error;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

@end
