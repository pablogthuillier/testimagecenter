//
//  Picture+Model.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Picture.h"

extern NSString *const pictureEntityName;
extern NSString *const nameProperty;
extern NSString *const sizeProperty;
extern NSString *const dateProperty;
extern NSString *const urlProperty;

@interface Picture (Model)

+ (instancetype)insertPictureInMOC:(NSManagedObjectContext *)context withDict:(NSDictionary *)dict;
+ (NSArray *)fetchAllPicturesInMOC:(NSManagedObjectContext *)context;

@end
