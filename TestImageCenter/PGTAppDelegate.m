//
//  PGTAppDelegate.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTAppDelegate.h"
#import "PGTViewController.h"
#import "Picture+Model.h"

NSString *const databaseName = @"database.data";
NSString *const initialDataFileName = @"initialData";
NSString *const plistExtension = @"plist";

@interface PGTAppDelegate ()

// I've chosen UIManagedDocument because it creates a complete stack in background and foreground for Core Data
@property (nonatomic,strong) UIManagedDocument *managedDocument;

@end

@implementation PGTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeViewControllers];
    return YES;
}
							


#pragma mark - Core Data stack

- (UIManagedDocument *)managedDocument{
    if(!_managedDocument){
        NSURL *fileURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:databaseName];
        _managedDocument = [[UIManagedDocument alloc] initWithFileURL:fileURL];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
            [_managedDocument openWithCompletionHandler:^(BOOL success){
                if (!success) {
                    // Handle the error.
                }
            }];
        }
        else {
            [_managedDocument saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success){
                [self insertInitialData];
                if (!success) {
                    // Handle the error.
                }
            }];
        }
        
    }
    
    return _managedDocument;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)insertInitialData {
    NSString *pathFilePictures = [[NSBundle mainBundle]pathForResource:initialDataFileName ofType:plistExtension];
    NSArray *arrayAuxPictures = [NSMutableArray arrayWithContentsOfFile:pathFilePictures];
    for(NSDictionary *dict in arrayAuxPictures){
        if([dict isKindOfClass:[NSDictionary class]]){
            [Picture insertPictureInMOC:self.managedDocument.managedObjectContext withDict:dict];
        }
        
    }
    
}

#pragma mark - Initialization

- (void)initializeViewControllers{
    UINavigationController *mainNavController = (UINavigationController *)self.window.rootViewController;
    PGTViewController *viewController = (PGTViewController *)mainNavController.viewControllers[0];
   
    viewController.managedDocument = self.managedDocument;
}



@end
