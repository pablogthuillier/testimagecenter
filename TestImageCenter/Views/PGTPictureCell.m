//
//  PGTPictureCell.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTPictureCell.h"

@interface PGTPictureCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *pictureNameLabel;

@end

@implementation PGTPictureCell


- (void)setViewModel:(PGTPictureViewModel *)viewModel{
    _viewModel = viewModel;
    [self loadData];
}

- (void)loadData {
    self.pictureNameLabel.text = self.viewModel.pictureName;
    self.pictureImageView.image = self.viewModel.pictureImage;
}



@end
