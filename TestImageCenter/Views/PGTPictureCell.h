//
//  PGTPictureCell.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGTPictureViewModel.h"

@interface PGTPictureCell : UICollectionViewCell

@property (nonatomic,strong) PGTPictureViewModel *viewModel;



@end
