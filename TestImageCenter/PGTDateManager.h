//
//  PGTDateManager.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGTDateManager : NSObject

+ (NSString *)dateWithDayMonthAndYear:(NSDate *)date;
+ (NSString *)localStringFromDate:(NSDate *)date;

@end
