//
//  PGTPictureViewModel.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 30/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTPictureViewModel.h"
#import "PGTDateManager.h"

@implementation PGTPictureViewModel

- (NSString *)pictureName{
    return self.picture.name;
}

- (UIImage *)pictureImage{
    UIImage *image = nil;
    // Picture can be located in documents directory if it is downloaded
    // or in main bundle if is a initial picture
    
    if(self.picture.url && ![self.picture.url isEqualToString:@""]){
        image = [self pictureFromDocumentDirectoryWithURL:self.picture.url];
        if(!image){
            image = [UIImage imageNamed:self.picture.url];
        }
        
    }
    
    return image;
}

- (NSString *)pictureDate{
    return [PGTDateManager dateWithDayMonthAndYear:self.picture.date];
}

- (NSUInteger)pictureSize{
    return [self.picture.size integerValue];
}


#pragma mark - Auxiliary

- (UIImage *)pictureFromDocumentDirectoryWithURL:(NSString *)url{
    UIImage *image = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:url];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]){
        image = [UIImage imageWithContentsOfFile:path];
    }
    
    return image;

}

@end
