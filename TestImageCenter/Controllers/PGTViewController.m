//
//  PGTViewController.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTViewController.h"
#import "PGTPictureLibraryViewController.h"
#import "PGTDownloaderViewController.h"

NSString *const openLibrarySegue = @"openLibrary";
NSString *const openDownloaderSegue = @"openDownloader";
NSString *const viewControllerTitle = @"Picture Manager";

@interface PGTViewController ()

@end

@implementation PGTViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self setTitle:viewControllerTitle];
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:openLibrarySegue]){
        PGTPictureLibraryViewController *destination = (PGTPictureLibraryViewController *)[segue destinationViewController];
        destination.managedDocument = self.managedDocument;
    }
    
    else if([[segue identifier] isEqualToString:openDownloaderSegue]){
        PGTDownloaderViewController *destination = (PGTDownloaderViewController *)[segue destinationViewController];
        destination.managedDocument = self.managedDocument;
    }
    
    
}

@end
