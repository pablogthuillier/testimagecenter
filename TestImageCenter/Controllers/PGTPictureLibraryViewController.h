//
//  PGTPictureLibraryViewController.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTPictureLibraryViewController : UICollectionViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@end
