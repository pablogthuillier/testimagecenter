//
//  PGTImageDetailsViewController.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTImageDetailsViewController.h"

@interface PGTImageDetailsViewController () <UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *pictureFullImageView;
@property (weak, nonatomic) IBOutlet UILabel *pictureNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pictureSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pictureDateLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *pictureScrollView;

@property (nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic,strong) UIPinchGestureRecognizer *pinchGestureRecognizer;

@end

@implementation PGTImageDetailsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
    [self setupView];
}



- (void)loadData {
    self.pictureFullImageView.image = self.viewModel.pictureImage;
    self.pictureNameLabel.text = self.viewModel.pictureName;
    self.pictureSizeLabel.text = [NSString stringWithFormat:@"%lu KB",self.viewModel.pictureSize];
    self.pictureDateLabel.text = self.viewModel.pictureDate;
}

- (void)setupView {
    
    self.pictureScrollView.delegate = self;
    self.pictureScrollView.minimumZoomScale = 1.0;
    self.pictureScrollView.maximumZoomScale = 40.0;
    
}


- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    //I've chosen a scrollview for scroll and pan the image instead of using two gesture recognizers
    return self.pictureFullImageView;
}


@end
