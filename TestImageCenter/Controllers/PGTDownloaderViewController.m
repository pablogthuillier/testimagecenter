//
//  PGTDownloaderViewController.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 31/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDownloaderViewController.h"
#import "Picture+Model.h"

@interface PGTDownloaderViewController ()
@property (weak, nonatomic) IBOutlet UITextField *imageURLTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@end

@implementation PGTDownloaderViewController

- (IBAction)downloadPushed:(id)sender {
    
    NSString *urlString = self.imageURLTextField.text;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *data = [self dataFromURLString:urlString];
        
        NSString *name = self.nameTextField.text;
        NSNumber *size = @(data.length / 1024);
        NSString *fileName = [self fileNameFromURLString:urlString];
        
        // I'm saving in Documetns Directory root, but path could be given by a directory and a filename
        NSString *filePath = fileName;
        
        [self saveData:data ToFilePath:filePath];
        
        NSDictionary *dict = @{nameProperty:name,
                               sizeProperty:size,
                               urlProperty:fileName};
        dispatch_async(dispatch_get_main_queue(), ^{
            //TODO: Send a post notification for observing the change from library view, and then reload the collection view
            [Picture insertPictureInMOC:self.managedDocument.managedObjectContext withDict:dict];
        });
    });
    
}

#pragma mark - Auxiliary

- (NSData *)dataFromURLString:(NSString *)urlString {
    // It would be better to use a class for managing connections to internet (it is usually used in many classes)
    // This class would recieve a block that would be executed when data is downloaded
    // It would be great too to give some feedback to user when picture is downloaded
    NSURL *url = [NSURL URLWithString:urlString];
    return [NSData dataWithContentsOfURL:url];
}

- (NSString *)fileNameFromURLString:(NSString *)urlString {
    NSArray *listSlashes = [urlString componentsSeparatedByString:@"/"];
    return [listSlashes lastObject];
}


-(void)saveData:(NSData *)data ToFilePath:(NSString *)filePath {
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    
    NSString *saveFilePath = [docPath stringByAppendingPathComponent:filePath];
    [data writeToFile:saveFilePath atomically:YES];
    
    
}


@end
