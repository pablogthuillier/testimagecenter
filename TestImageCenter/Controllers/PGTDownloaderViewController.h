//
//  PGTDownloaderViewController.h
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 31/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTDownloaderViewController : UIViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@end
