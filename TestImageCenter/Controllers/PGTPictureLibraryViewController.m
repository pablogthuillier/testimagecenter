//
//  PGTPictureLibraryViewController.m
//  TestImageCenter
//
//  Created by Pablo González Thuillier on 29/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTPictureLibraryViewController.h"
#import "Picture+Model.h"
#import "PGTPictureCell.h"
#import "PGTPictureViewModel.h"
#import "PGTImageDetailsViewController.h"


NSString *const cellName = @"cellPicture";
NSString *const openPictureDetailsSegue = @"openPictureDetailsSegue";

@interface PGTPictureLibraryViewController ()

@property (nonatomic,copy) NSArray *pictures;

@end

@implementation PGTPictureLibraryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Picture Library"];
    [self initializeArray];
}


- (void)initializeArray {
    self.pictures = [Picture fetchAllPicturesInMOC:self.managedDocument.managedObjectContext];
    [self.collectionView reloadData];
}

#pragma mark - Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.pictures count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Picture *picture = self.pictures[indexPath.row];
    
    PGTPictureCell *cell = (PGTPictureCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellName forIndexPath:indexPath];

    // I use a ViewModel to delegate the model description task outside the controller.
    // I just send the model to viewmodel and the viewmodel tells the cell what to show
    PGTPictureViewModel *viewModel = [[PGTPictureViewModel alloc] init];
    viewModel.picture = picture;
    
    cell.viewModel = viewModel;
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:openPictureDetailsSegue]){
        
        NSIndexPath *pathForSelectedItem = [self.collectionView indexPathsForSelectedItems][0];
        
        Picture *picture = self.pictures[pathForSelectedItem.row];
        
        PGTImageDetailsViewController *destination = (PGTImageDetailsViewController *)[segue destinationViewController];
        PGTPictureViewModel *viewModel = [[PGTPictureViewModel alloc] init];
        viewModel.picture = picture;
        destination.viewModel = viewModel;
    }
}


@end
